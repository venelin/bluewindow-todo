<?php

use Bluewindow\ToDo\Repositories\TaskRepository;
use Bluewindow\ToDo\Data\TaskDTO;
use Bluewindow\ToDo\Database\Database;

if (!class_exists('WP_CLI')) {
	return;
}

WP_CLI::add_command('bwtodo:task-get-one', function($args, $assoc_args) {
	$id = $assoc_args['id'] ?? 0;
	$task = (new TaskRepository())->get_one($id);

}, ['shortdesc' => 'Gets one Task']);


WP_CLI::add_command('bwtodo:task-get-all', function() {
	$results = (new TaskRepository())->get_all();

}, ['shortdesc' => 'Gets all Tasks']);


WP_CLI::add_command('bwtodo:task-find-by-title', function($args, $assoc_args) {
	$title = $assoc_args['title'] ?? '';
	$results = (new TaskRepository())->find_by_title($title);

}, ['shortdesc' => 'Finds Tasks by title']);


WP_CLI::add_command('bwtodo:task-find-one-by-user', function($args, $assoc_args) {
	$id = $assoc_args['id'] ?? 0;
	$user_id = $assoc_args['user_id'] ?? 0;
	$task = (new TaskRepository())->find_one_by_user($id, $user_id);

}, ['shortdesc' => 'Finds a task by User ID']);

WP_CLI::add_command('bwtodo:task-find-by-user', function($args, $assoc_args) {
	$user_id = $assoc_args['user_id'] ?? 0;
	$results = (new TaskRepository())->find_by_user($user_id);

}, ['shortdesc' => 'Finds a task by User ID']);


WP_CLI::add_command('bwtodo:delete-task', function($args, $assoc_args) {
	$id = $assoc_args['id'] ?? 0;
	$result = (new TaskRepository())->delete($id);

}, ['shortdesc' => 'Deletes a Task']);

WP_CLI::add_command('bwtodo:insert-task', function() {
	$task = new TaskDTO(
		-1,
		'Lorem ipsum',
		'Lorem ipsum discripsum',
		1
	);

	(new TaskRepository())->insert($task);

}, ['shortdesc' => 'Inserts a Task']);

WP_CLI::add_command('bwtodo:update-task', function() {
	$task = new TaskDTO(
		14,
		'Updated title',
		'Updated Description',
		1
	);

	(new TaskRepository())->update($task);

}, ['shortdesc' => 'Updates a Task']);
