<?php

namespace Bluewindow\ToDo\Controllers\Ajax;

use Bluewindow\ToDo\Controllers\Ajax\Task\TaskController;

class AjaxController {
	public function register_handlers() {
		add_action('wp_ajax_bwtodo_task_preview', [TaskController::class, 'preview']);

		add_action('wp_ajax_bwtodo_task_insert_view', [TaskController::class, 'insert_view']);

		add_action('wp_ajax_bwtodo_task_insert', [TaskController::class, 'insert']);

		add_action('wp_ajax_bwtodo_task_update_view', [TaskController::class, 'update_view']);

		add_action('wp_ajax_bwtodo_task_update', [TaskController::class, 'update']);

		add_action('wp_ajax_bwtodo_task_delete_view', [TaskController::class, 'delete_view']);

		add_action('wp_ajax_bwtodo_task_delete', [TaskController::class, 'delete']);

		add_action('wp_ajax_bwtodo_task_search_title', [TaskController::class, 'find_by_title']);
	}
}
