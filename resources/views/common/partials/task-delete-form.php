<?php

if (empty($task)) {
	return;
}

$id = !empty($task) ? $task->get_id() : '';
$user_id = get_current_user_id();
$action = 'bwtodo_task_delete';
?>

<h2>
	<?php esc_html_e( $task->get_title() ); ?>
</h2>

<h3>
	<?php _e('Are you sure you want to delete this task?', BW_TODO_TEXTDOMAIN); ?>
</h3>

<form id="bwtodo-task-delete-form" action="?" method="post" class="bwtodo-form js-bwtodo-task-action-form">
	<input type="hidden" name="id" value="<?php echo esc_attr($id); ?>" />
	<input type="hidden" name="action" value="<?php echo esc_attr($action); ?>">
	<?php wp_nonce_field('bwtodo_ajax_nonce', '_bwtodo_nonce'); ?>

	<div class="form__actions form__actions--center">
		<button class="button bwtodo-btn bwtodo-btn-delete bwtodo-btn-large">
			<?php _e('Delete', BW_TODO_TEXTDOMAIN); ?>
		</button>
	</div><!-- /.form__actions -->
</form>
