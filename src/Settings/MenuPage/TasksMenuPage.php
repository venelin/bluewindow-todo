<?php

namespace Bluewindow\ToDo\Settings\MenuPage;

use Bluewindow\ToDo\Repositories\TaskRepository;

class TasksMenuPage {
	public function add() {
		add_menu_page(
			__( 'Tasks', BW_TODO_TEXTDOMAIN ),
			__( 'To-Do List', BW_TODO_TEXTDOMAIN ),
			'read',
			'bwtodo-tasks',
			[$this, 'html'],
			'dashicons-list-view'
		);
	}

	public function html() {
		if (!current_user_can('read')) {
			return;
		}

		$current_user_id = get_current_user_id();
		$tasks = (new TaskRepository())->find_by_user($current_user_id);

		bwtodo_render_view('admin/page/tasks', compact('tasks'));
	}
}
