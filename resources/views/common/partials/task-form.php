<?php

$has_task = !empty($task);
$btn_label = $has_task ? __('Edit Task', BW_TODO_TEXTDOMAIN) : __('Add Task', BW_TODO_TEXTDOMAIN);
$action = $has_task ? 'bwtodo_task_update' : 'bwtodo_task_insert';
$id = !empty($task) ? $task->get_id() : '';
$title = !empty($task) ? $task->get_title() : '';
$description = !empty($task) ? $task->get_description() : '';
$user_id = get_current_user_id();
?>

<form id="bwtodo-task-form" action="?" method="post" class="bwtodo-form js-bwtodo-task-action-form">
	<input type="hidden" name="id" value="<?php echo esc_attr($id); ?>" />
	<input type="hidden" name="action" value="<?php echo esc_attr($action); ?>">
	<?php wp_nonce_field('bwtodo_ajax_nonce', '_bwtodo_nonce'); ?>

	<div class="form__row">
		<label for="bwtodo-task-title">
			<?php _e('Title', BW_TODO_TEXTDOMAIN); ?>
		</label>

		<input class="form__field" id="bwtodo-task-title" type="text" name="title" value="<?php echo esc_attr($title) ?>" />
	</div><!-- /.form__row -->

	<div class="form__row">
		<label for="bwtodo-task-description">
			<?php _e('Description', BW_TODO_TEXTDOMAIN); ?>
		</label>

		<textarea class="form__field" id="bwtodo-task-description" name="description" rows="4"><?php esc_html_e($description) ?></textarea>
	</div><!-- /.form__row -->

	<div class="form__actions">
		<button class="button bwtodo-btn bwtodo-btn-large">
			<?php _e($btn_label, BW_TODO_TEXTDOMAIN); ?>
		</button>
	</div><!-- /.form__actions -->
</form>
