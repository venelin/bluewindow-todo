<?php

namespace Bluewindow\ToDo\Repositories;

use Bluewindow\ToDo\Data\TaskDTO;
use Bluewindow\ToDo\Database\Database;

class TaskRepository {
	protected const TABLE_NAME = 'tasks';
	private $db;
	private $wpdb;
	private $full_table_name;

	public function __construct() {
		$db = Database::instance();
		$this->db = $db;
		$this->wpdb = $db->wpdb;
		$this->full_table_name = $db->full_table_prefix . static::TABLE_NAME;
	}

	public function get_one(int $id) {
		$row = $this->wpdb->get_row(
			$this->wpdb->prepare(
				"SELECT * from {$this->full_table_name} WHERE `id` = %d",
				[$id]
			)
		);

		return $row ? $this->map_row_to_dto($row) : null;
	}

	public function get_all() {
		$results = $this->wpdb->get_results(
			"SELECT * from {$this->full_table_name}",
		);

		return array_map(function($row) {
			return $this->map_row_to_dto($row);
		}, (array)$results);
	}

	public function find_by_title(int $user_id, string $title) {
		$results = $this->wpdb->get_results(
			$this->wpdb->prepare(
				"SELECT * from {$this->full_table_name} WHERE `title` LIKE %s AND `user_id` = %d",
				['%' . $title . '%', $user_id]
			)
		);

		return array_map(function($row) {
			return $this->map_row_to_dto($row);
		}, (array)$results);
	}

	public function find_one_by_user(int $id, int $user_id) {
		$row = $this->wpdb->get_row(
			$this->wpdb->prepare(
				"SELECT * from {$this->full_table_name} WHERE `id` = %d AND `user_id` = %d",
				[$id, $user_id]
			)
		);

		return $row ? $this->map_row_to_dto($row) : null;
	}

	public function find_by_user(int $user_id) {
		$results = $this->wpdb->get_results(
			$this->wpdb->prepare(
				"SELECT * from {$this->full_table_name} WHERE `user_id` = %d",
				[$user_id]
			)
		);

		return array_map(function($row) {
			return $this->map_row_to_dto($row);
		}, (array)$results);
	}

	public function insert(TaskDTO $task) {
		return $this->wpdb->insert(
			$this->full_table_name,
			[
				'title' => $task->get_title(),
				'description' => $task->get_description(),
				'user_id' => $task->get_user_id()
			], [
				'%s',
				'%s',
				'%d'
			]
		);
	}

	public function update(TaskDTO $task) {
		return $this->wpdb->update(
			$this->full_table_name,
			[
				'title' => $task->get_title(),
				'description' => $task->get_description(),
				'user_id' => $task->get_user_id()
			], [
				'id' => $task->get_id()
			], [
				'%s',
				'%s',
				'%d'
			]
		);
	}

	public function delete(int $id) {
		return $this->wpdb->delete(
			$this->full_table_name,
			[
				'id' => $id
			], [
				'%d'
			]
		);
	}

	private function map_row_to_dto($row) {
		return new TaskDTO(
			$row->id ?? null,
			$row->title ?? null,
			$row->description ?? null,
			$row->user_id ?? null
		);
	}
}
