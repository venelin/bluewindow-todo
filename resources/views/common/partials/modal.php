<div id="bwtodo-modal" class="bwtodo-modal js-bwtodo-modal">
	<span id="bwtodo-modal-close-btn" class="bwtodo-modal-close-btn js-bwtodo-modal-close-btn">
		<img src="<?php echo BW_TODO_ASSETS_URL; ?>/images/x-icon.png" alt="" />
	</span>

	<div class="modal__content js-bwtodo-modal-content">
		<!-- modal content goes here -->
	</div><!-- /.modal__content -->
</div>

<div id="bwtodo-modal-backdrop" class="bwtodo-modal-backdrop js-bwtodo-modal-backdrop"></div>
