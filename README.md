# Bluewindow ToDo

### Description

To-Do List Management Solution for WordPress.
The plugin allows users to manage their own tasks.

### Requirements

The plugin requires:
- PHP Version 7.4 or up

### Installation

You need to install the plugin manually.

- Log into your site and in the WordPress admin
- Go to the Plugins page and click Add New
- On the “Add Plugins” page, click Upload Plugin
- Select the plugin ZIP file, and click Install Now.

Alternatively, if you cannot upload plugins directly to WordPress.

- Unzip the file, a folder with the plugin's name will be created
- Upload the unzipped folder to your site’s ../wp-content/plugins directory
- Go back to the Plugins page and activate the plugin


## Using the BW Todo List

### Managing Tasks

The tasks can be managed from the To-do List menu page.
Or you can add the shortcode [bwtodo-tasks-table] in the content of any page, and the same task management table will appear.

#### Adding a New Task

- Click the `Add New` button.
- Enter the task title and description.
- Click `Save Task`.

#### Editing a Task

- Choose a task you'd like to edit and click the `Edit` button.
- Make your change and click `Edit Task`

#### Deleting a Task:

- Simply click on the `Delete` button on the task you'd like to delete.


### Shortcodes


#### [bwtodo-tasks-table]
- Use the shortcode `[bwtodo-tasks-table]` in any page or post to display the current user's Tasks.
- Each user will see, and be able to edit only their own tasks.
