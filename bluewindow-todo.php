<?php
/*
 * Plugin Name:       Bluewindow To-do
 * Plugin URI:        https://bluewindowltd.com/
 * Description:       To-do list application for allowing users to manage their own tasks
 * Version:           0.0.1
 * Requires PHP:      7.2
 * Author:            Bluewindow
 * Author URI:        https://bluewindowltd.com/
 * License:           GPL v2 or later
 * License URI:       https://www.gnu.org/licenses/gpl-2.0.html
 * Text Domain:       bluewindow-todo
 * Domain Path:       /languages
 */

use Bluewindow\ToDo\ToDo;

if (!defined('ABSPATH')) {
	return;
}

define('BW_TODO_PLUGINS_DIR', dirname(__DIR__));
define('BW_TODO_PLUGIN_DIR', plugin_dir_path(__FILE__));
define('BW_TODO_PLUGIN_URL', plugin_dir_url(__FILE__));
define('BW_TODO_ASSETS_URL', BW_TODO_PLUGIN_URL . 'assets/');
define('BW_TODO_PLUGIN_FILE', __FILE__);
define('BW_TODO_VIEW_DIR', __DIR__ . DIRECTORY_SEPARATOR . 'resources' . DIRECTORY_SEPARATOR . 'views' . DIRECTORY_SEPARATOR);

define('BW_TODO_BASENAME', plugin_basename(__FILE__));
define('BW_TODO_PHP_MIN_VERSION', '7.4');
define('BW_TODO_TEXTDOMAIN', 'bluewindow-todo');

if (BW_TODO_PHP_MIN_VERSION > phpversion()) {
    deactivate_plugins(BW_TODO_BASENAME);
	wp_die(__('The plugin "Bluewindow To-do" has been deactivated because it requires a minimum PHP version of ' . BW_TODO_PHP_MIN_VERSION, BW_TODO_TEXTDOMAIN));
}

require_once __DIR__.'/vendor/autoload.php';

load_plugin_textdomain('bluewindow-todo', false, dirname(plugin_basename(__FILE__)) . '/languages');

$toDo = new ToDo();
$toDo->boot();
