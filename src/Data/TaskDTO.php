<?php

namespace Bluewindow\ToDo\Data;

class TaskDTO {
	private int $id;
	private string $title;
	private string $description;
	private int $user_id;

	public function __construct(
		int $id,
		string $title,
		string $description,
		int $user_id
	) {
		$this->set_id($id);
		$this->set_title($title);
		$this->set_description($description);
		$this->set_user_id($user_id);
	}

	public function get_id() {
		return $this->id;
	}

	public function set_id(int $id) {
		$this->id = $id;

		return $this;
	}

	public function get_title() {
		return $this->title;
	}

	public function set_title(string $title) {
		$this->title = $title;

		return $this;
	}

	public function get_description() {
		return $this->description;
	}

	public function set_description(string $description) {
		$this->description = $description;

		return $this;
	}

	public function get_user_id() {
		return $this->user_id;
	}

	public function set_user_id(int $user_id) {
		$this->user_id = $user_id;

		return $this;
	}
}
