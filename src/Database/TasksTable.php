<?php

namespace Bluewindow\ToDo\Database;

class TasksTable extends DatabaseTable {
	protected const TABLE_NAME = 'tasks';

	public function __construct() {
		parent::__construct();
	}

	public function create() {
		if (!function_exists('dbDelta')) {
			throw new \LogicException('Function "dbDelta" does not exists');
		}
		
		$table_name = $this->full_table_name;
		$charset_collate = $this->charset_collate;
		$wp_prefix = $this->wpdb->prefix;

		$sql = "CREATE TABLE IF NOT EXISTS $table_name (
			id bigint(20) unsigned NOT NULL AUTO_INCREMENT,
			user_id bigint(20) unsigned NOT NULL,
			title varchar(255) NOT NULL,
			description text,
			created_at datetime DEFAULT CURRENT_TIMESTAMP NOT NULL,
			updated_at datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP NOT NULL,
			PRIMARY KEY (id),
			KEY user_id (user_id),
			CONSTRAINT fk_user_id FOREIGN KEY (user_id) REFERENCES {$wp_prefix}users(ID)
		) $charset_collate;";
	
		dbDelta($sql);
	}

	public function delete() {
		$table_name = $this->full_table_name;

		$sql = "DROP TABLE IF EXISTS $table_name";

		$this->wpdb->query($sql);
	}
}
