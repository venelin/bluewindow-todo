<?php

namespace Bluewindow\ToDo\Settings;

use Bluewindow\ToDo\Repositories\TaskRepository;

class BlockRegistrar {
	public function register() {
		add_action('init', [$this, 'blocks']);
	}

	public function blocks() {
		register_block_type( BW_TODO_PLUGIN_DIR . '/blocks/build/tasks-table/' );
	}
}
