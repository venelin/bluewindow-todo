<?php

namespace Bluewindow\ToDo\Database;

abstract class DatabaseTable {
	protected const PLUGIN_TABLE_PREFIX = 'bwtodo_';
	protected $wpdb;
	protected $charset_collate;
	protected $full_table_name;

	public function __construct() {
		global $wpdb;
		$this->wpdb = $wpdb;
		$this->charset_collate = $wpdb->get_charset_collate();
		$this->full_table_name = $wpdb->prefix . self::PLUGIN_TABLE_PREFIX . static::TABLE_NAME;
	}

	protected abstract function create();

	protected abstract function delete();
}
