<div id="bwtodo">
	<h3>
		<?php _e('To-do List', BW_TODO_TEXTDOMAIN); ?>
	</h3>

	<?php if (is_user_logged_in()): ?>
		<a href="#" class="button bwtodo-btn js-bwtodo-task-modal-btn" data-action="bwtodo_task_insert_view">
			<?php _e('Add New', BW_TODO_TEXTDOMAIN); ?>
		</a>

		<?php
		$tasks = !empty($tasks) ? $tasks : [];
		bwtodo_render_view('common/partials/task-filters');

		bwtodo_render_view('common/partials/table', compact('tasks'));

		bwtodo_render_view('common/partials/modal');
		?>
	<?php else : ?>
		<p>
			<?php _e('You need to be logged-in to Add, Edit and Delete your own tasks.', BW_TODO_TEXTDOMAIN); ?>
		</p>

		<p>
			<?php
			echo sprintf(
				'%s <a href="%s">%s</a>',
				__('You can log-in or register', BW_TODO_TEXTDOMAIN),
				admin_url(),
				__('here', BW_TODO_TEXTDOMAIN)
			);
			?>
		</p>
	<?php endif ?>
</div><!-- /#bwtodo -->
