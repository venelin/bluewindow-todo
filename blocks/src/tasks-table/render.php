<?php
/**
 * Render.php
 *
 * @see https://github.com/WordPress/gutenberg/blob/trunk/docs/reference-guides/block-api/block-metadata.md#render
 *
 * @package block-development-examples
 */

use Bluewindow\ToDo\Repositories\TaskRepository;

$current_user_id = get_current_user_id();
$tasks = (new TaskRepository())->find_by_user($current_user_id);

bwtodo_render_view('public/shortcodes/tasks-table', compact('tasks'));
