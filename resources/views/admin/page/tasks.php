<div id="bwtodo">
	<h1>
		<?php _e('To-do List', BW_TODO_TEXTDOMAIN); ?>
	</h1>

	<a href="#" class="button bwtodo-btn js-bwtodo-task-modal-btn" data-action="bwtodo_task_insert_view">
		<?php _e('Add New', BW_TODO_TEXTDOMAIN); ?>
	</a>

	<?php
	$tasks = !empty($tasks) ? $tasks : [];

	bwtodo_render_view('common/partials/task-filters');

	bwtodo_render_view('common/partials/table', compact('tasks'));

	bwtodo_render_view('common/partials/modal');
	?>
</div><!-- /#bwtodo -->
