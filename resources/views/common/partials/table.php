<div class="bwtodo-task-table-wrapper js-bwtodo-task-table-wrapper">
	<?php if (!empty($tasks)): ?>
		<table id="bwtodo-task-table" class="bwtodo-task-table wp-list-table widefat fixed striped js-bwtodo-task-table">
			<thead>
			</thead>

			<tbody>
				<?php foreach ($tasks as $task): ?>
					<tr data-task-id="<?php echo esc_attr($task->get_id()) ?>">
						<td class="table__col-title"><?php esc_html_e( $task->get_title() ); ?></td>
						<td class="table__col-description"><?php esc_html_e( $task->get_description() ); ?></td>
						<td class="table__col-actions">
							<a href="#" class="button bwtodo-btn bwtodo-btn-success js-bwtodo-task-modal-btn" data-action="bwtodo_task_preview">
								<?php _e('View', BW_TODO_TEXTDOMAIN); ?>
							</a>
							<a href="#" class="button bwtodo-btn bwtodo-btn-info js-bwtodo-task-modal-btn" data-action="bwtodo_task_update_view">
								<?php _e('Edit', BW_TODO_TEXTDOMAIN); ?>
							</a>
							<a href="#" class="button bwtodo-btn bwtodo-btn-delete js-bwtodo-task-modal-btn" data-action="bwtodo_task_delete_view">
								<?php _e('Delete', BW_TODO_TEXTDOMAIN); ?>
							</a>
						</td>
					</tr>
				<?php endforeach ?>
			</tbody>
		</table>	
	<?php else : ?>
		<h3>
			<?php _e('No tasks', BW_TODO_TEXTDOMAIN); ?>
		</h3>
	<?php endif ?>
</div><!-- /.bwtodo-task-table-wrapper -->
