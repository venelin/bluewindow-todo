<?php return array(
    'root' => array(
        'name' => 'bluewindow/todo',
        'pretty_version' => 'dev-main',
        'version' => 'dev-main',
        'reference' => '5381da6c41942739086aaa04da688315e97c4ffc',
        'type' => 'library',
        'install_path' => __DIR__ . '/../../',
        'aliases' => array(),
        'dev' => true,
    ),
    'versions' => array(
        'bluewindow/todo' => array(
            'pretty_version' => 'dev-main',
            'version' => 'dev-main',
            'reference' => '5381da6c41942739086aaa04da688315e97c4ffc',
            'type' => 'library',
            'install_path' => __DIR__ . '/../../',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
    ),
);
