<?php

namespace Bluewindow\ToDo\Controllers\Scripts;

class ScriptsController {
	public function enqueue() {
		add_action('admin_enqueue_scripts', [$this, 'enqueue_admin_scripts']);
		add_action('wp_enqueue_scripts', [$this, 'enqueue_public_scripts']);
	}

	public function enqueue_admin_scripts($hook_sufix) {
		if (strpos($hook_sufix, 'bwtodo') === false) {
			return;
		}

		wp_enqueue_script('bwtodo-main', BW_TODO_ASSETS_URL . '/common/js/main.js', ['jquery'], false, true);
		wp_localize_script('bwtodo-main', 'bwtodoData', [
			'ajaxUrl' => admin_url('admin-ajax.php')
		]);

		wp_enqueue_style('bwtodo-styles', BW_TODO_ASSETS_URL . '/common/css/styles.css', [], '1.0.0');
	}

	public function enqueue_public_scripts() {
		wp_enqueue_script('bwtodo-main', BW_TODO_ASSETS_URL . '/common/js/main.js', ['jquery'], false, true);
		wp_localize_script('bwtodo-main', 'bwtodoData', [
			'ajaxUrl' => admin_url('admin-ajax.php')
		]);

		wp_enqueue_style('common');
		wp_enqueue_style('list-tables');
		wp_enqueue_style('forms');
		wp_enqueue_style('core');
		wp_enqueue_style('bwtodo-styles', BW_TODO_ASSETS_URL . '/common/css/styles.css', [], '1.0.0');
	}
}
