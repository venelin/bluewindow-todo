<?php

namespace Bluewindow\ToDo\Exceptions;

class ViewNotFoundException extends \Exception {
}
