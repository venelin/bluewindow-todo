<?php

if (empty($task)) {
	return;
}
?>

<h1>
	<?php esc_html_e( $task->get_title() ); ?>
</h1>

<p>
	<?php echo nl2br(esc_html($task->get_description())); ?>
</p>
