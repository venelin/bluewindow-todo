(function($) {

if (typeof bwtodoData == 'undefined') {
	return;
}

const ajaxUrl = bwtodoData.ajaxUrl;
const $doc = $(document);
const $taskTable = $('.js-bwtodo-task-table');
const $modal = $('.js-bwtodo-modal');
const $modalBackdrop = $('.js-bwtodo-modal-backdrop');
const $modalCloseBtn = $('.js-bwtodo-modal-close-btn');
const $modalContentContainer = $modal.find('.modal__content');
const fadeOutFastDuration = 50;
const fadeOutDuration = 300;

let doingAjax = false;

// Close Modal Events
$modalBackdrop.on('click', closeModal);
$modalCloseBtn.on('click', closeModal);
$doc.on('keydown', function(e) {
	if (e.key === 'Escape' || e.keyCode === 27) {
		closeModal();
	}
});

// Open Popup Modal Event
$doc.on('click', '.js-bwtodo-task-modal-btn', function(e) {
	e.preventDefault();

	if (doingAjax) {
		return;
	}

	const $this = $(this);
	const action = $this.attr('data-action');
	const taskId = $this.closest('[data-task-id]').attr('data-task-id');

	doingAjax = true;
	$.ajax({
		url: ajaxUrl,
		method: 'get',
		data: [{
			name: 'action',
			value: action
		},{
			name: 'id',
			value: taskId
		}],
		success: function(response) {
			if (response.success && response.data.html) {
				modalUpdateContent(response.data.html);
				openModal();
			}
		},
		error: function(error) {
			console.log( error );
		},
		complete: function () {
			doingAjax = false;
		}
	});
});

// Submit Task Action Form
$doc.on('submit', '.js-bwtodo-task-action-form', function(e) {
	e.preventDefault();

	if (doingAjax) {
		return;
	}

	let data = $(this).serializeArray();

	doingAjax = true;
	$.ajax({
		url: ajaxUrl,
		method: 'post',
		data: data,
		success: function(response) {
			if (response.success && response.data.html) {
				$('.js-bwtodo-task-table-wrapper').replaceWith(response.data.html);
				closeModal();
			}
		},
		error: function(error) {
			console.log( error );
		},
		complete: function () {
			doingAjax = false;
		}
	});
});

function closeModal() {
	$modal.fadeOut(fadeOutFastDuration);
	$modalBackdrop.fadeOut(fadeOutFastDuration);
}

function openModal() {
	$modalBackdrop.show();
	$modal.show();
}

function modalUpdateContent(content) {
	$modalContentContainer.html(content);
}

})(jQuery);
