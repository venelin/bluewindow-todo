<?php

namespace Bluewindow\ToDo\Controllers\Ajax\Task;

use Bluewindow\ToDo\Repositories\TaskRepository;
use Bluewindow\ToDo\Data\TaskDTO;

class TaskController {
	public static function preview() {
		$id = (int)bwtodo_request_param('id', 0);
		$task = (new TaskRepository())->get_one($id);

		wp_send_json_success([
			'html' => bwtodo_render_view(
				'common/partials/modal-preview',
				compact('task'),
				false
			)
		]);
	}

	public static function insert_view() {
		wp_send_json_success([
			'html' => bwtodo_render_view(
				'common/partials/task-form',
				[],
				false
			)
		]);
	}

	public static function insert() {
		check_ajax_referer('bwtodo_ajax_nonce', '_bwtodo_nonce');

		$title = bwtodo_request_param('title', '');
		$description = bwtodo_request_param('description', '');
		$user_id = (int)get_current_user_id();

		$task = new TaskDTO(
			-1,
			$title,
			$description,
			$user_id
		);

		$result = (new TaskRepository())->insert($task);

		if (!empty($result)) {
			$tasks = (new TaskRepository())->find_by_user($user_id);

			wp_send_json_success([
				'html' => bwtodo_render_view(
					'common/partials/table',
					compact('tasks'),
					false
				)
			]);
		} else {
			wp_send_json_error([
				'message' => __('The Task could not be added.', BW_TODO_TEXTDOMAIN)
			]);
		}
	}

	public static function update_view() {
		$id = (int)bwtodo_request_param('id', 0);
		$task = (new TaskRepository())->get_one($id);

		wp_send_json_success([
			'html' => bwtodo_render_view(
				'common/partials/task-form',
				compact('task'),
				false
			)
		]);
	}

	public static function update() {
		check_ajax_referer('bwtodo_ajax_nonce', '_bwtodo_nonce');

		$id = (int)bwtodo_request_param('id', 0);
		$user_id = (int)get_current_user_id();
		if (!self::user_can_edit($id, $user_id)) {
			wp_die('Not allowed to edit this task.');
		}

		$title = bwtodo_request_param('title', '');
		$description = bwtodo_request_param('description', '');

		$task = new TaskDTO(
			$id,
			$title,
			$description,
			$user_id
		);

		$result = (new TaskRepository())->update($task);

		if (!empty($result)) {
			$tasks = (new TaskRepository())->find_by_user($user_id);

			wp_send_json_success([
				'html' => bwtodo_render_view(
					'common/partials/table',
					compact('tasks'),
					false
				)
			]);
		} else {
			wp_send_json_error([
				'message' => __('The Task could not be editted.', BW_TODO_TEXTDOMAIN)
			]);
		}
	}

	public static function delete_view() {
		$id = (int)bwtodo_request_param('id', 0);
		$task = (new TaskRepository())->get_one($id);

		wp_send_json_success([
			'html' => bwtodo_render_view(
				'common/partials/task-delete-form',
				compact('task'),
				false
			)
		]);
	}

	public static function delete() {
		check_ajax_referer('bwtodo_ajax_nonce', '_bwtodo_nonce');

		$id = (int)bwtodo_request_param('id', '');
		$user_id = (int)get_current_user_id();
		if (!self::user_can_edit($id, $user_id)) {
			wp_die('Not allowed to delete this task.');
		}
		$result = (new TaskRepository())->delete($id);

		if (!empty($result)) {
			$tasks = (new TaskRepository())->find_by_user($user_id);

			wp_send_json_success([
				'html' => bwtodo_render_view(
					'common/partials/table',
					compact('tasks'),
					false
				)
			]);
		} else {
			wp_send_json_error([
				'message' => __('The Task could not be deleted.', BW_TODO_TEXTDOMAIN)
			]);
		}
	}

	private static function user_can_edit(int $id, int $user_id) {
		$task = (new TaskRepository())->find_one_by_user($id, $user_id);

		return !empty($task);
	}

	public static function find_by_title() {
		$title = bwtodo_request_param('search_title', 0);
		$user_id = (int)get_current_user_id();

		if (empty($title)) {
			$tasks = (new TaskRepository())->find_by_user($user_id);
		} else {
			$tasks = (new TaskRepository())->find_by_title($user_id, $title);
		}

		wp_send_json_success([
			'html' => bwtodo_render_view(
				'common/partials/table',
				compact('tasks'),
				false
			)
		]);
	}
}
