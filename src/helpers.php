<?php

use Bluewindow\ToDo\View;

if (!function_exists('bwtodo_view')) {
	function bwtodo_render_view(string $view, array $params = [], $echo = true) {
		if (!$echo) {
			return (new View($view, $params))->render();
		}

		(new View($view, $params))->render(true);
	}
}

if (!function_exists('bwtodo_request_param')) {
	function bwtodo_request_param($key = '', $default = null) {
		$value = false;
		if (!$key) {
			return $value;
		}

		if (isset($_POST[$key])) {
			$value = $_POST[$key];
		} elseif (isset($_GET[$key])) {
			$value = $_GET[$key];
		}

		if (!$value && !is_null($default)) {
			$value = $default;
		}

		return $value;
	}
}
