<div class="bwtodo-task-filters">
	<form action="" method="get" class="js-bwtodo-task-action-form">
		<input class="form__field" id="bwtodo-task-title-search" type="text" name="search_title" value="" />
		<input type="hidden" name="action" value="bwtodo_task_search_title">

		<button class="button bwtodo-btn">
			<?php _e('Search', BW_TODO_TEXTDOMAIN); ?>
		</button>
	</form>
</div>
