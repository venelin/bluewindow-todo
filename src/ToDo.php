<?php

namespace Bluewindow\ToDo;

use Bluewindow\ToDo\Database\TasksTable;
use Bluewindow\ToDo\Settings\MenuPage\TasksMenuPage;
use Bluewindow\ToDo\Settings\ShortcodeRegistrar;
use Bluewindow\ToDo\Settings\BlockRegistrar;
use Bluewindow\ToDo\Controllers\Ajax\AjaxController;
use Bluewindow\ToDo\Controllers\Scripts\ScriptsController;

class ToDo {
	public function boot() {
		register_activation_hook(BW_TODO_PLUGIN_FILE, [$this, 'create_database_tables']);
		add_action('admin_menu', [$this, 'add_settings_pages']);
		(new AjaxController)->register_handlers();
		(new ScriptsController)->enqueue();
		(new ShortcodeRegistrar)->register();
		(new BlockRegistrar)->register();
	}

	public function add_settings_pages() {
		(new TasksMenuPage)->add();
	}

	public function create_database_tables() {
		require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );

		(new TasksTable())->create();
	}
}
