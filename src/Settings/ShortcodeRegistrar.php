<?php

namespace Bluewindow\ToDo\Settings;

use Bluewindow\ToDo\Repositories\TaskRepository;

class ShortcodeRegistrar {
	public function register() {
		add_shortcode('bwtodo-tasks-table', [$this, 'tasks_table']);
	}

	public function tasks_table() {
		$current_user_id = get_current_user_id();
		$tasks = (new TaskRepository())->find_by_user($current_user_id);

		return bwtodo_render_view('public/shortcodes/tasks-table', compact('tasks'), false);
	}
}
